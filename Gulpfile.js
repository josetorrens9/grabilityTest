const gulp = require('gulp')
const sass = require('gulp-sass')
const rename = require('gulp-rename')
const babel = require('babelify')
const browserify = require('browserify')
const source = require('vinyl-source-stream')
const watchify = require('watchify')

gulp.task('styles', ()=>{
	gulp
		.src('index.scss')
		.pipe(sass())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('public'))
  gulp
    .src('media-queries.scss')
    .pipe(sass())
    .pipe(rename('app.css'))
    .pipe(gulp.dest('public'))    
})

gulp.task('images', () =>{
	gulp
		.src('assets/images/*')
		.pipe(gulp.dest('public/images'))
  gulp
    .src('assets/images/icons/*')
    .pipe(gulp.dest('public/images/icons'))    
})

gulp.task('js', () =>{
  gulp
    .src('assets/js/*')
    .pipe(gulp.dest('public/'))
})

gulp.task('fonts', () =>{
  gulp
    .src('assets/fonts/Open_Sans/*')
    .pipe(gulp.dest('public/fonts/Open_Sans'))   
  gulp
    .src('assets/fonts/roboto/*')
    .pipe(gulp.dest('public/fonts/roboto'))       
})

function compile(watch) {
  var bundle = watchify(browserify('./src/index.js', {debug: true}));

  function rebundle() {
    bundle
      .transform(babel)
      .bundle()
      .on('error', function (err) {
        console.log(err);
        this.emit('end')
      })
      .pipe(source('index.js'))
      .pipe(rename('app.js'))
      .pipe(gulp.dest('public'));
  }

  if (watch) {
    bundle.on('update', function () {
      console.log('--> Bundling...');
      rebundle();
    });
  }

  rebundle();
}

gulp.task('build', function () {
  return compile();
});

gulp.task('watch', function () { return compile(true); });

gulp.task('default', ['styles', 'images', 'js', 'fonts', 'build']);