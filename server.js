const express = require('express')
const app = express()
const api = require('marvel-api');
const marvel = api.createClient({
  publicKey: 'b5ebb168b8641e00a8c55e2d276542ca',
  privateKey: 'fac2875b3b77a76217f3fcb7aca1cf3485be180d'
});

app.set('view engine', 'pug');
app.use(express.static('public'));

app.get('/', (req, res) =>{
	res.render('index', {})
})

app.get('/api/chars/:page/:search/:sortBy', (req, res) =>{
	if(req.params.search!='-'){
		marvel.characters.findNameStartsWith(req.params.search, req.params.sortBy, 10, ((req.params.page-1)*10))
		  .then((response)=>{
		  	res.send(response)
		  })
		  .fail(console.error)
		  .done();	
	}else{
		marvel.characters.findAll(req.params.sortBy, 10, (req.params.page*10))
		  .then((response)=>{
		  	res.send(response)
		  })
		  .fail(console.error)
		  .done();			
	}
})

app.get('/api/comics/:id', (req, res) =>{
	
	marvel.comics.find(req.params.id)
	  .then((response)=>{
	  	res.send(response)
	  })
	  .fail(console.error)
	  .done();

})

app.get('*', (req, res) =>{
	res.render('index', {})
})

app.listen(8000, (err) => {
	if(err) return console.log('Crash'), process.exit(1)

	console.log('GrabilityTest corriendo en el 8000')
})