const request = require('axios')
const localBD = require('../localStorage')

const bd = new localBD()

getCharacters =  (page, search, sortBy, callback) => {
  if(search==='')
    search='-'
  if(sortBy==='')
    sortBy='name'  
  
  request.get(`/api/chars/${page}/${search}/${sortBy}`)
    .then( (res) => {
      callback(res.data);
    })
    .catch( (err) => {
      console.log(err);
      callback({data:[]});
    })
}

getComicDetail =  (comicId, callback) => {
  request.get('/api/comics/'+comicId)
    .then( (res) => {
      callback(res.data, bd.findComic(res.data.data[0].id))
    })
    .catch( (err) => {
      console.log(err);
    })
}

module.exports = getCharacters;