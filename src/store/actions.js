const loading = require('../components/loading')
const detailContent = require('../components/comic-detail')
const api = require('../store/api')
const localBD = require('../localStorage')

const bd = new localBD()

openModal =  (comicId) => {
  $("#modal").modal({
      dismissible: true,
      opacity: .5,
      in_duration: 300,
      out_duration: 200,
      starting_top: '4%',
      ending_top: '10%',
      ready: (modal, trigger) => {
      	$("#modal .modal-body").html("").append(loading())
      	getComicDetail(comicId, (data, added) => {
      		$("#modal .modal-body").html("").append(detailContent(data.data[0], added))
      	});

      },
      complete: () => {$("#modal .modal-body").html("")}
    }
  );	
	$("#modal").modal('open');
}

addToLocalStorage = (newComic, favItem) => {
	let local = bd.getList();
	local.push(newComic)
	bd.replaceList(local)
	$('.favourite-list').append(favItem(newComic))	
}

deleteFromLocalStorage = (comic, favItem) => {
	let local = bd.getList()	
	local.forEach((item, index)=>{
		if(item.id==comic.id)
			local.splice(index, 1);
	})

	bd.replaceList(local)
	
	$('.favourite-list').html("")
	
	local.forEach((item)=>{
		$('.favourite-list').append(favItem(item))
	})
}


addToFavourites = (comic, favItem) => {
	if(!bd.findComic(comic.id)){
		let newComic=bd.setObject(comic);
		addToLocalStorage(newComic, favItem)
		$("#modal .modal-body").html("").append(detailContent(comic, true))
		Materialize.toast(newComic.title+' has been added', 1000)
	}
	else
		Materialize.toast('This comics is already in the list of favorites', 1000)
}

deleteFromModal = (comic, favItem) => {
	var local = bd.getList()	
	if(bd.findComic(comic.id)){
		deleteFromLocalStorage(comic, favItem)
		$("#modal .modal-body").html("").append(detailContent(comic, false))
		Materialize.toast(comic.title+' has been deleted', 1000)
	}
	else
		Materialize.toast('This comics is already in the list of favorites', 1000)
}

deleteFromFavourites = (favourite, favItem) => {
	var local = bd.getList()	
	if(bd.findComic(favourite.id)){
		deleteFromLocalStorage(favourite, favItem)
		Materialize.toast(favourite.title+' has been deleted', 1000)
	}
	else
		Materialize.toast('This comics is not on the list of favorites', 1000)
}

showMenu = () => {
	$('#overlay').toggleClass('on-med-show')
	$('body').toggleClass('bodyToggle')
	$('.right-side').toggleClass('show-list')
}

addRandomComics = (list, favItem) => {
	var comics = setGeneralComicsList(list.data),
		count=0;

	while (count < 3) {
		let random = Math.floor(Math.random() * ((comics.length+1) - 0)) + 0;
		var comicId=comics[random].resourceURI.split('comics/')[1];
		if(!bd.findComic(comicId)){
			getComicDetail(comicId, (data) => {
				let newComic=bd.setObject(data.data[0]);
				addToLocalStorage(newComic, favItem)
				Materialize.toast('We\'ve randomly added comics to your list, find out!', 1000)
			});			
			count++;
		}
	}

}

setGeneralComicsList = (list) => {
	var result = []
	list.forEach((item) => {
		if(item.comics.returned>0){
			let comics = item.comics.items;
			comics.forEach((comic) =>{
				result.push(comic);
			})
		}
	})
	return (result);
}