function local () {
    "use strict";
  
  this.setObject = function (comic) {
    return {id:comic.id, 
            title:comic.title, 
            image: comic.images.length > 0 ? comic.images[0].path+"."+comic.images[0].extension : 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg',
            }
  }

  this.findComic = function (id) {
    var local = JSON.parse(localStorage.favourites)    
    var exists=false;
    local.forEach((item)=>{
      if(item.id==id)
        exists=true;  
    })
    return exists;
  }

  this.replaceList = function (list){
    localStorage.favourites = JSON.stringify(list)
  }

  this.getList = function () {
    if(localStorage.favourites)
      return JSON.parse(localStorage.favourites)
    else
      return JSON.parse(localStorage.favourites=JSON.stringify([]))
  }
}


module.exports = local;
