const yo = require('yo-yo')
const favItem = require('../favourite-item')

module.exports = function (favourites) {
	return yo`<div class="favourite-list">
				${favourites.map( (favourite) => {
					return favItem(favourite)
				})}														
			</div>`;
}					
