const yo = require('yo-yo')
const actions = require('../../store/actions')

function detailContent (comic, added) {
	let comicInfo = getInfo(comic)
	var favItem = require('../favourite-item')
	if(added){
		return yo`<div>
					<div class="modal-content">
						<div class='row'>
							<div class='col s12 m4'>
								<img src="${comicInfo.image}" alt="">
							</div>
							<div class='col s12 m8'>
								<div class='title'>${comicInfo.title}</div>
								<p>
									${comicInfo.description}
								</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">					
						<div class='col s12 m6 btn-comic-detail btn-added' onclick=${deleteFromModal.bind(null, comic, favItem)}>
							<img src="/images/icons/btn-favourites-primary.png" alt="">
							<span>Added to favourites</span>
						</div>	
						<div class='col s12 m6 btn-comic-detail btn-buy'>
								<img src="/images/icons/shopping-cart-primary.png" alt="">
								<span>Buy for $${comicInfo.price}</span>
						</div>							
					</div>
				</div>`;
	}
	else{	
		return yo`<div>
					<div class="modal-content">
						<div class='row'>
							<div class='col s12 m4'>
								<img src="${comicInfo.image}" alt="">
							</div>
							<div class='col s12 m8'>
								<div class='title'>${comicInfo.title}</div>
								<p>
									${comicInfo.description}
								</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">					
						<div class='col s12 m6 btn-comic-detail btn-add' onclick=${addToFavourites.bind(null, comic, favItem)}>
							<img src="/images/icons/btn-favourites-default.png" alt="">
							<span>Add to favourites</span>
						</div>	
						<div class='col s12 m6 btn-comic-detail btn-buy'>
								<img src="/images/icons/shopping-cart-primary.png" alt="">
								<span>Buy for $${comicInfo.price}</span>
						</div>							
					</div>
				</div>`;	
	}
}

module.exports = detailContent;

function getInfo (comic) {
	var info={
		title: comic.title,
		description: comic.description ? comic.description : 'Description not available',
		image: comic.images.length > 0 ? comic.images[0].path+"."+comic.images[0].extension : 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg',
		price: comic.prices.length > 0 ? comic.prices[0].price : '0'		
	}
	return info
}
