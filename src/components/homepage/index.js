const page = require('page');
const empty = require('empty-element');
const header = require('../header');
const template = require('./template');

page('/', queryStringToJSON, header, function (ctx, next) {

  $('title').html('GrabilityTest');

  var main = document.getElementById('main-container');
  
  empty(main).appendChild(template(ctx.query.page, ctx.query.search, ctx.query.sortBy, false));
})

page('*', queryStringToJSON, header, function (ctx, next) { 
  var main = document.getElementById('main-container');
  empty(main).appendChild(template(ctx.query.page, ctx.query.search, ctx.query.sortBy, true));
})

function queryStringToJSON(ctx, next) {
	var queryString = ctx.querystring
  if(queryString.indexOf('?') > -1){
    queryString = queryString.split('?')[1];
  }
  var pairs = queryString.split('&');
  var result = {};
  pairs.forEach(function(pair) {
    pair = pair.split('=');
    if(pair[0]!="" && pair[1]!="")
    	result[pair[0]] = decodeURIComponent(pair[1] || '');
  });
  ctx.query=result;
  if(ctx.query.page==null){ctx.query.page=1}
  if(ctx.query.search==null){ctx.query.search=""}
  if(ctx.query.sortBy==null){ctx.query.sortBy=""}    
  
  next();
}