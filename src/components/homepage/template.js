var yo = require('yo-yo');
var layout = require('../layout');
var rigth = require('../rigth-side');
var left = require('../left-side');

module.exports = function (page, search, sortBy, notFound) {
	var el = yo`<div class='row content-wrapper'>
					${left(page, search, sortBy, notFound)}
					${rigth}
					<div id="modal" class="modal modal-fixed-footer comic-detail">
						<img src='/images/icons/btn-close.png' class="modal-action modal-close waves-effect btn-close">
						<div class='modal-body'></div>
					</div>						
				</div>`;
	return layout(el);
}
