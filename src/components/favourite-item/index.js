const yo = require('yo-yo')
const actions = require('../../store/actions')

var favItem = function (favourite) {
	return yo `<div class="favourite-item">
					<img class='btn-delete waves-effect waves-teal' src='/images/icons/btn-delete.png' onclick=${deleteFromFavourites.bind(null, favourite, favItem)} >
					<img src="${favourite.image}" onclick=${openModal.bind(null, favourite.id)} alt="">
					<span>
						${favourite.title}
					</span>
				</div>`;
}

module.exports = favItem;