const yo = require('yo-yo')
const list = require('../comics-list');

module.exports = function (character) {
	if(character.comics.items.length>0){
		return yo`<div class="col s12 m6">
					  	<div class="card char-card">
					    	<div class="card-content">
				            	<figure>	
									<img src="${character.thumbnail.path}.${character.thumbnail.extension}" alt="" class="circle">		            			
				            	</figure>
				            	<div class='char-info'>
						        	<span class="char-name">${character.name}</span>
						        	<p>${character.description.length > 2 ? character.description: 'Description not available'}</p>
						        	<a class="waves-effect waves-teal btn-flat btn-view-more" onclick=${reveal}>View more</a>
				            	</div>								
					        </div>
					        <div class="card-action">
								<div class="row">
									<span class="title">Related comics</span>
								</div>
								<div class="row">
									<ul>
										${character.comics.items.map((comic, index)=>{
											if(index<4)
												return list(comic, character.id)
										})}
									</ul>
								</div>									
					        </div>
						    <div class="card-aditional-data">
								<div class="row">
									<span class="title">Related comics</span>
									<img src='/images/icons/btn-close.png' class="waves-effect btn-close" onclick=${reveal}>
								</div>
								<div class="row">
									<ul>
										${character.comics.items.map((comic, index)=>{
											return list(comic, character.id)
										})}
									</ul>
								</div>	
						    </div>				        
					    </div>			    
					</div>`;
	}
	else{
		return yo`<div class="col s12 m6">
					  	<div class="card char-card">
					    	<div class="card-content">
				            	<figure>	
									<img src="${character.thumbnail.path}.${character.thumbnail.extension}" alt="" class="circle">		            			
				            	</figure>
				            	<div class='char-info'>
						        	<span class="char-name">${character.name}</span>
						        	<p>${character.description}</p>
						        	<a class="waves-effect waves-teal btn-flat btn-view-more" onclick=${reveal}>View more</a>
				            	</div>								
					        </div>
					        <div class="card-action">
								<div class="row">
									<span class="title">Related comics</span>
								</div>
								<div class="row">
									<div class="row">
										<span class='no-display' >No related comics to display</span>
									</div>	
								</div>									
					        </div>
						    <div class="card-aditional-data">
								<div class="row">
									<span class="title">Related comics</span>
									<img src='/images/icons/btn-close.png' class="waves-effect btn-close" onclick=${reveal}>
								</div>
								<div class="row">
									<span class='no-display' >No related comics to display</span>
								</div>	
						    </div>				        
					    </div>			    
					</div>`;		
	}
}

function reveal (el) {
	console.log(this)
	var card=$(this).parent().parent().parent();
	if(!card.hasClass('overflow')){
		card.toggleClass('overflow')
		setTimeout(function() {card.find( ".card-aditional-data" ).css({"-webkit-transform":"translateY(-100%)"})}, 0);
		
	}
	else{
		card.find( ".card-aditional-data" ).css({"-webkit-transform":"translateY(0%)"})
		card.toggleClass('overflow', 10)
	}
}