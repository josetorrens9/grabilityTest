const yo = require('yo-yo')
const card = require('../character-card');
const actions = require('../../store/actions')
const randomBtn = require('../random-btn')
const noRecords = require('../no-records')

module.exports = function (page, search, sortBy, characters, notFound) {
	if(!notFound && characters.data.length>0){
		addRandomBtn(characters);
		var totalPages=Math.round((characters.meta.total/10)+0.4);
		return yo`<div class='grid'>
					<div class='row'>	
						${characters.data.map( (character ) => {
							return card(character)
						})}
					</div>				
					<ul class="pagination">
						${leftArrow(page, search, sortBy)}
						${setPaginationControls(page, totalPages, sortBy).map( (item) =>{
							if(!item.href)
								return yo ` <li class="${item.clasS} number"><a>${item.number}</a></li> `;
							else{
								var href = '/?page='+item.number;
								if(search!=''){
									href=href+'&search='+search
								}
								if(sortBy!='')
									href=href+'&sortBy='+sortBy
								
								return yo ` <li class="${item.clasS} number"><a href="${href}">${item.number}</a></li> `;
							}
						})}							
						${rigthArrow(page, totalPages, search, sortBy)}
					</ul>
				</div>`;		
	}else{
		return noRecords;
	}

}					

function leftArrow (page, search, sortBy) {
	if(page==1)
		return yo `<li class="disabled"><a class='left-arrow'></a></li>`
	else{
		var href = '/?page='+(parseInt(page)-1);
		if(search!=''){
			href=href+'&search='+search
		}
		if(sortBy!='')
			href=href+'&sortBy='+sortBy		

		return yo `<li class="waves-effect"><a href="${href}" class='left-arrow'></a></li>`
	}	
}

function rigthArrow (page, total, search, sortBy) {
	var href = '/?page='+(parseInt(page)+1);
	if(search!=''){
		href=href+'&search='+search
	}
	if(sortBy!='')
		href=href+'&sortBy='+sortBy
	if(page<total)
		return yo `<li class="waves-effect"><a href="${href}" class='rigth-arrow'></a></li>`	
	else
		return yo `<li class="disabled"><a class='rigth-arrow'></a></li>`
}

function setPaginationControls (page, total, sortBy) {
	var i=1, list=[];
	if(page>5)
		i = +page-4

	for(i;i<=total;i++){
		if(i == page)
			list.push({number:i, clasS:'active'})
		else
			list.push({number:i, clasS:'waves-effect', href:true})
		if (i == (+page + 4)){
			list.push({number:'...', clasS:'disabled'})
			break;
		}
	}
	return list;
}

function addRandomBtn (list) {
	$('header').append(randomBtn(list))
}