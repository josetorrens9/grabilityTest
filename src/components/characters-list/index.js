const empty = require('empty-element');
const template = require('./template');
const loading = require('../loading');
const api = require('../../store/api')
const noRecords = require('../no-records')

module.exports = function (page, search, sortBy, notFound) {

  if(!notFound){
    setTimeout(function () {
      $('#characters-list').html("").append(loading)
      backToTop();
      getCharacters(page, search, sortBy, (data)=>{
        $('#characters-list').html("").append(template(page, search, sortBy, data, notFound));
      })
    }, 100)
  }
  else{
    setTimeout(()=>{
      $('#characters-list').html("").append(noRecords);
    }, 100)
  }
}

function backToTop (argument) {
  $('body,html').animate({
    scrollTop: 0
  }, 800);
  return false;
}