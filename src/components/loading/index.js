const yo = require('yo-yo')

module.exports = function () {
	return yo`<div class='loading'>
				<img src='/images/loading.gif'>
			</div>`;
}