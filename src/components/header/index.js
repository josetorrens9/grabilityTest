const yo = require('yo-yo');
const page = require('page')
const empty = require('empty-element');
const actions = require('../../store/actions')

var el = function(query){
 	return yo`<header class="navbar-fixed">
				<nav>
					<div class="nav-wrapper">
						<a href="/" class="brand-logo"><img src="/images/marvel.jpg" alt=""></a>
						<div class="center on-med-right">
							<input id="search" type="search" onkeypress=${getNewList.bind(null, query)} value='${query.search}' placeholder='Search character...'>
						</div>
					</div>
				</nav>
            </header>`;	
}

module.exports = function header (ctx, next) {
  var container = document.getElementById('header-container')
  empty(container).appendChild(el(ctx.query));
  next();
}

function getNewList (query, e) {
	if(e.which==13){
		if($('#search').val()==''){
			page('/')
			e.preventDefault()
		}
		else if(query.search!=$('#search').val()){
			page('/?search='+$('#search').val())
			e.preventDefault()
		}
	}
}
