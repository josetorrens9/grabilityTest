const yo = require('yo-yo')
const favourites = require('../favourites-list')
const actions = require('../../store/actions')

module.exports = yo`<div class='col s12 m3 right-side'>
						<section>
							<div class='title'>
								<img src='/images/icons/favourites.png' alt=''>
								<div class='btn-list on-med-show' onclick=${showMenu.bind()}></div>
								<span>My favourites</span>
							</div>
						</section>
						<section id='favourite-list'>
							${favourites}
						</section>						
					</div>`;	
