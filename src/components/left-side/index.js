const yo = require('yo-yo')
const characters = require('../characters-list')
const actions = require('../../store/actions')
const page = require('page')

module.exports = function (page, search, sortBy, notFound){ 
			return yo`<div class='col s12 m9 left-side'>
						<section>
							<div class='row title'>
								<div class='col s8 m6 l8'>
									<div class=''>
										<img src='/images/icons/characters.png' alt=''>
										<span>Characters</span>
									</div>						
								</div>
								<div class='col s12 m4 l4'>
									<div class='rigth'>
									  <select id='sort' class='browser-default' value='${sortBy}' onchange=${getNewList.bind(null, search, sortBy, this)}>
									    <option value="" disabled selected>Sort by</option>
									    <option value="name">Name (Asc)</option>
									    <option value="-name">Name (Desc)</option>
									  </select>
									</div>				
								</div>				
								<div class='btn-list fixed on-med-show' onclick=${showMenu.bind()}></div>
							</div>
						</section>
						
						<section id='characters-list'>
							${characters(page, search, sortBy, notFound)}
						</section>					
					</div>`;
}

function getNewList (search, sortBy, e) {
	let newSort=$('#sort').val()
	if($('#search').val()==''){
		page(`/?sortBy=${newSort}`)
		e.preventDefault()
	}
	else{
		page(`/?search=${search}&sortBy=${newSort}`)
		e.preventDefault()
	}
}